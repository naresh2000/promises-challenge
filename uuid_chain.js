const fs = require('fs');
const { uuid } = require('uuidv4');

const filePath = './chain.txt';



const generateUUID = () => {
  return new Promise((resolve) => {
    resolve(uuid());
  });
};

const writeFile = (data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, 'utf8', (err) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

const readFile = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

const convertToUppercase = (data) => {
  return new Promise((resolve) => {
    const uppercaseData = data.toUpperCase();
    resolve(uppercaseData);
  });
};

// Usage
generateUUID()
  .then((uuid) => writeFile(uuid))
  .then(() => readFile())
  .then((data) => convertToUppercase(data))
  .then((uppercaseData) => writeFile(uppercaseData))
  .then(() => console.log('UUID v4 generated, converted to uppercase, and written to file successfully!'))
  .catch((error) => console.error('Error:', error));
