const { checkPrime } = require('crypto')
const fs = require('fs')

function checkPermissons(path){
    return new Promise((reslove, reject) => {
        fs.stat(path,(err,stats) => {
            if (err)
                reject(err.message)
            else{
                const mode = stats.mode;
                const binaryMode = mode.toString(2).slice(-9)
                let permissions ={
                    user : binaryMode.slice(0,3),
                    group : binaryMode.slice(3,6),
                    others : binaryMode.slice(6,9)
                }
                
                reslove(permissions)
            }
        })
    })
}

checkPermissons('./permissions.txt')
.then((res) => {
    console.log(res)
})